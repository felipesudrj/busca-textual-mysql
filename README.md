# Busca textual Mysql

Como realizar uma busca textual de forma eficiente no Mysql

Primeiro, é necessário utilizar a função levenshtein_ratio() 
ela verifica uma palavra passada e quanto % de changes ser o que estou buscando em relação as palavras encontradas no banco de dados.

No exemplo abaixo eu tenho uma tabela de veiculos, uma de marcas e outra de modelos.
Passando uma string, eu posso identificar os veículos daquele modelo da string mesmo que não possua todas as palavras extamente na busca.
Exemplo, eu quero buscar um carro crossfox mas vamos supor que o usuário digite o nome crosfox.... ou crosfoxi o scrip irá localizar mesmo assim o modelo desejado.