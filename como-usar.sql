SELECT 
* 
FROM veiculo WHERE veiculo.veiculo_marca_modelo_id = (
SELECT 
    vmm.id
    
FROM
    veiculo v
    INNER JOIN veiculo_marca vm ON vm.id = v.veiculo_marca_id
    INNER JOIN veiculo_marca_modelo vmm ON vmm.id = v.veiculo_marca_modelo_id
    WHERE levenshtein_ratio(vmm.nome,'fiat')
ORDER BY levenshtein_ratio(vmm.nome,'fiat') DESC LIMIT 1
)
